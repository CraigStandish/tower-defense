﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseTower : MonoBehaviour {

	//Grpahics
	public Sprite icon;
	public GameObject projectile;

	//Spells
	//Inital List of Prefabs to Initalize
	public List<GameObject> Spells = new List<GameObject>();

	public List<BaseSpell> Spellbook = new List<BaseSpell>();
	public List<BaseSpell> KnownSpells = new List<BaseSpell>();

	//Tower Info
	public int id;

	//Targets
	public List<BaseEnemy> Targets = new List<BaseEnemy>();

	public float CastFinished;
	public Combat_Text CombatText;
	public GameObject Castbar;
	public BaseSpell Casting;

	public float SanityTime;

	public bool IsCasting()
	{
		return Time.time <= SanityTime;
	}

	public void InvokeGCD()
	{
	foreach (BaseSpell spell in KnownSpells) 
		{
		if (spell.CooldownRemaining() > 0)
			{
				spell.cooldowndone = Time.time + .5f;
			}
		}
	}

	public BaseSpell HasSpell(string name)
	{
		return KnownSpells.Find(x => x.name.Equals(name));
		}

	public BaseSpell HasSpell(int id)
	{
		return KnownSpells.Find(x => x.id == id);
	}

	// Use this for initialization
	public virtual void Start () {
		CombatText = GameObject.FindGameObjectWithTag ("Combat Text").GetComponent<Combat_Text> ();
	//Initalize Spells
		foreach (GameObject spell in Spells) {
			Debug.Log (spell);
			GameObject TempSpell = Instantiate(spell, transform.position, Quaternion.identity) as GameObject;
			TempSpell.transform.parent = this.transform;
			TempSpell.GetComponent<BaseSpell>().Caster = this.gameObject;
			Spellbook.Add(TempSpell.GetComponent<BaseSpell>());
			if (TempSpell.GetComponent<BaseSpell>().Unlocked)
			{
				KnownSpells.Add(TempSpell.GetComponent<BaseSpell>());
			}
				}
		//KnownSpells.Add (Spellbook [0]);
		//KnownSpells.Add (Spellbook [1]);
	}
	
	// Update is called once per frame
	public virtual void Update () {
		HandleCombat ();
	}

	public virtual void HandleCombat()
	{

	}

}
