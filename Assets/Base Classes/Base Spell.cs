﻿using UnityEngine;
using System.Collections;

public class BaseSpell : MonoBehaviour {

	//Grahpics
	public Sprite icon;
	public GameObject projectile;

	public string name;
	public string description;
	public int id;

	public int mindamage;
	public int maxdamage;
	public float critchance;
	public float critseverity;

	public enum Elements
	{
		Physical,
		Fire,
		Cold,
		Lightning
	}
	public Elements element;

	public float casttime;
	public float cooldown;

	//Internal Variables
	public float cooldowndone;
	public GameObject Caster;

	public bool Unlocked;

	public Combat_Text CombatText;

	//Methods

	public float CooldownRemaining()
	{
		return cooldowndone - Time.time;
	}

	public void Awake()
	{
	}

	// Use this for initialization
	public void Start () {
		description = "Deals " + mindamage + " - " + maxdamage + " " + element + " damage";
		CombatText = GameObject.FindGameObjectWithTag ("Combat Text").GetComponent<Combat_Text> ();
	}
	
	// Update is called once per frame
	public void Update () {
	}

	public virtual void PreCast()
	{
		Caster.GetComponent<BaseTower> ().CastFinished = Time.time + casttime;
		Caster.GetComponent<BaseTower> ().SanityTime = Time.time + casttime;
				//FOR NOW INVOKE CD HERE LATER INVOKE WHEN PROJECTILE OR SPELL IS ACTUALLY FIRED
		cooldowndone = Time.time + cooldown;
		PostCast ();
	}

	public virtual void PostCast()
	{
		Debug.Log ("IN POST CAST FOR SPELL " + name);
		if (projectile == null) {
			int damage = Random.Range(mindamage,maxdamage);
						ApplyDamage (damage, Caster.GetComponent<BaseTower> ().Targets [0]);
				}
		else {
			GameObject Projectile = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
			Projectile.GetComponent<Base_Projectile>().Target = Caster.GetComponent<BaseTower>().Targets[0].gameObject;
			Projectile.transform.parent = this.gameObject.transform;
			Debug.Log ("WE HAVE PROJECTILE!");
				}
		Caster.GetComponent<BaseTower> ().Casting = null;
	}

	public virtual void ApplyDamage()
	{
		int damage = Random.Range (mindamage, maxdamage);
		GameObject target = Caster.GetComponent<BaseTower> ().Targets [0].gameObject;
		CombatText.ShowText (icon, damage.ToString (), target);
		target.GetComponent<BaseEnemy>().hp -= damage;
		}

	public virtual void ApplyDamage(int damage, BaseEnemy target)
	{
		CombatText.ShowText (icon, damage.ToString (), target.gameObject);
		target.hp -= damage;
	}

	public virtual bool IsReady()
	{
		return (!Caster.GetComponent<BaseTower>().IsCasting() && Time.time > cooldowndone);
	}

}
