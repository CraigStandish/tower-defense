﻿using UnityEngine;
using System.Collections;

public class RangeHelper : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnTriggerEnter(Collider c)
	{
		Debug.Log ("ENTERED");
		if (c.gameObject.tag == "Ground Enemy" || c.gameObject.tag == "Flying Enemy") {
			transform.parent.GetComponent<BaseTower>().Targets.Add(c.gameObject.GetComponent<BaseEnemy>());
				}
	}

	public void OnTriggerExit(Collider c)
	{
		if (c.gameObject.tag == "Ground Enemy" || c.gameObject.tag == "Flying Enemy") {
			transform.parent.GetComponent<BaseTower>().Targets.Remove(c.gameObject.GetComponent<BaseEnemy>());
		}
	}

}
