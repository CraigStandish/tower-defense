﻿using UnityEngine;
using System.Collections;

public class Base_Projectile : MonoBehaviour {

	public float speed;

	public GameObject Target;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, step);
		if (Vector3.Distance (this.transform.position, Target.transform.position) < .5f) {
			transform.parent.GetComponent<BaseSpell>().ApplyDamage();
			Destroy (this.gameObject);
				}
	}
}
