﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Skillslot : MonoBehaviour {

	public BaseSpell MySpell;
	public GameObject Tooltip;

	// Use this for initialization
	void Start () {
		Tooltip.SetActive (false);
	}

	public void Awake()
	{
		Tooltip = GameObject.FindGameObjectWithTag ("Spell Tooltip");
	}
	
	// Update is called once per frame
	void Update () {
	if (MySpell != null) {
			transform.GetChild(0).GetComponent<Image>().sprite = MySpell.icon;

			if (MySpell.GetComponent<BaseSpell>().CooldownRemaining() > 0)
			{
				transform.GetChild(1).gameObject.SetActive(true);
				transform.GetChild(1).GetComponent<Text>().text = MySpell.CooldownRemaining().ToString("f0");
			}
			else
			{
				transform.GetChild(1).gameObject.SetActive(false);
			}

				}
	}

	public void onMouseEnter()
	{
		if (MySpell != null) {
			Tooltip.SetActive(true);
			Tooltip.transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y+50f, 0);
						Tooltip.transform.GetChild (0).GetComponent<Text> ().text = MySpell.name;
						Tooltip.transform.GetChild (1).GetComponent<Text> ().text = MySpell.description;
				}
	}

	public void OnMouseExit()
	{
		Tooltip.SetActive (false);
	}

	public void OnClick()
	{
		if (MySpell != null && !MySpell.Unlocked) {
			Debug.Log ("CLICKED A LEARNABLE SPELL");
				}
	}

}
