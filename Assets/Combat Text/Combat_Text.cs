﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Combat_Text : MonoBehaviour {

	public GameObject CombatText;
	public GameObject Canvas;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShowText(string text, GameObject target)
	{

	}

	public void ShowText(Sprite icon, string text, GameObject target)
	{
		GameObject texter = Instantiate (CombatText, transform.position, Quaternion.identity) as GameObject;
		texter.transform.parent = Canvas.transform;
		texter.GetComponent<Text_Scroll> ().target = target;
		texter.transform.GetChild (0).GetComponent<Image> ().sprite = icon;
		texter.transform.GetChild (1).GetComponent<Text> ().text = text;
	}

}
