﻿using UnityEngine;
using System.Collections;

public class Text_Scroll : MonoBehaviour {

	public GameObject target;
	public float endtime;

	// Use this for initialization
	void Start () {
		transform.position = Camera.main.WorldToScreenPoint (target.transform.position);
		transform.position = new Vector3 (transform.position.x + 20f, transform.position.y+10f, 0f);
		endtime = Time.time + 1.5f;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (transform.position.x, transform.position.y + (Time.deltaTime*15), 0f);
		if (Time.time > endtime) {
			Destroy(this.gameObject);
				}
	}
}
