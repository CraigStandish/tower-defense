﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Fire_Tower : BaseTower {

	// Use this for initialization
	void Start () {
		base.Start ();
	}
	
	// Update is called once per frame
	void Update () {
		base.Update ();
	}

	public override void HandleCombat()
	{
		if (Targets.Count > 0) 
		{
			if (HasSpell("Fire Blast") && HasSpell("Fire Blast").IsReady())
			{
				HasSpell("Fire Blast").PreCast();
				Casting = HasSpell("Fire Blast");
			}

			if (HasSpell("Fireball") && HasSpell("Fireball").IsReady())
			{
				HasSpell("Fireball").PreCast();
				Casting = HasSpell("Fireball");
				Castbar.GetComponent<Slider>().minValue = Time.time;
				Castbar.GetComponent<Slider>().maxValue = Time.time + HasSpell("Fireball").casttime;
			}
		}
	}
}
