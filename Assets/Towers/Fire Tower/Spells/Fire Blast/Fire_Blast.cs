﻿using UnityEngine;
using System.Collections;

public class Fire_Blast : BaseSpell {

	// Use this for initialization
	void Start () {
		base.Start ();
	}
	
	// Update is called once per frame
	void Update () {
		base.Update ();
	}

	void Awake()
	{
		base.Awake ();
	}

	public override void PreCast()
	{
		base.PreCast ();
		PostCast ();
	}

}
