﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Grid_Tower_Control : MonoBehaviour {

	public GameObject SkillBar;
	public GameObject LastHitGrid;
	public GameObject SelectedTower;
	public GameObject BaseTower;

	public LayerMask GridMask;

	public GameObject Castbar;
	public GameObject MainCanvas;

	public int curid = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;

		LastHitGrid = null;

		if (Physics.Raycast (ray, out hit, 1000, GridMask)) {
			LastHitGrid = hit.collider.gameObject;
			if (Input.GetMouseButtonDown(0))
			{
				HandleLeftClick(LastHitGrid);
			}
				}
	}

	public void HandleLeftClick(GameObject hit)
	{
		Debug.Log (hit.name);
		if (hit.collider.gameObject.tag == "Open Grid") {
			GameObject TempTower = Instantiate(BaseTower, hit.transform.position, Quaternion.identity) as GameObject;
			GameObject TempCastbar = Instantiate(Castbar, hit.transform.position, Quaternion.identity) as GameObject;
			TempCastbar.GetComponent<Castbar>().MyTower = TempTower.GetComponent<BaseTower>();
			TempTower.GetComponent<BaseTower>().Castbar = TempCastbar;
			TempCastbar.transform.parent = MainCanvas.transform;
			hit.tag = "Closed Grid";
			hit.GetComponent<Grid>().MyTower = TempTower;
			TempTower.GetComponent<BaseTower>().id = curid;
			curid++;
			TempTower.transform.parent = hit.transform;
			TempTower.transform.localPosition = new Vector3(0,5,0);
		} else {
						SelectedTower = hit.collider.gameObject.GetComponent<Grid> ().MyTower;
						for (int i = 0; i < SelectedTower.GetComponent<BaseTower>().Spellbook.Count; i++) {
								SkillBar.transform.GetChild (i).GetComponent<Skillslot> ().MySpell = SelectedTower.GetComponent<BaseTower> ().Spellbook [i];
				if (!SelectedTower.GetComponent<BaseTower> ().Spellbook [i].Unlocked)
				{
					SkillBar.transform.GetChild (i).transform.GetChild(2).gameObject.SetActive(true);
				}
						}
				}
	}

}
