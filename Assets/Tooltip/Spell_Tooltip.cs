﻿using UnityEngine;
using System.Collections;

public class Spell_Tooltip : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShowTooltip()
	{
		this.gameObject.SetActive (true);
	}

	public void HideTooltip()
	{
		this.gameObject.SetActive (false);
	}

}
