﻿using UnityEngine;
using System.Collections;

public class BaseEnemy : MonoBehaviour {

	public float speed;
	public int hp;
	public int maxhp;

	// Use this for initialization
	public virtual void Start () {
		maxhp = hp;
	}
	
	// Update is called once per frame
	public virtual void Update () {
	
	}
}
