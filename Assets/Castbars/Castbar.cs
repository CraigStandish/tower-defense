﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Castbar : MonoBehaviour {

	public BaseTower MyTower;

	// Use this for initialization
	void Start () {
		if (MyTower != null) {
						Vector3 wantedpos = Camera.main.WorldToScreenPoint (MyTower.transform.position);
			wantedpos = new Vector3(wantedpos.x,wantedpos.y + 20f, 0f);
						transform.position = wantedpos;
				}
	}
	
	// Update is called once per frame
	void Update () {
	if (Time.time < this.gameObject.GetComponent<Slider> ().maxValue) {		
						this.gameObject.GetComponent<Slider> ().value += Time.deltaTime;
				} else {

				}
	}
}
